# Array based Math Library

A linear algebra math library based on Rust array objects.
This library is implemented using procedural macros and uses loop unrolling to achieve performant
operations on small vectors and matrices via the [unroll](https://gitlab.com/elrnv/unroll.git) crate.

## Features

  - [x] Scalar trait implemented for common numeric primitives.
  - [x] Fixed size array based vectors
  - [x] Fixed size array based square matrices
    - [x] Scalar multiplication
    - [x] Vector multiplication
    - [x] Matrix multiplication
    - [x] Vectorize
    - [x] Determinant
    - [x] Transpose
    - [x] Trace
    - [x] Frobenius Norm
    - [ ] Invert (only 2D and 3D matrices for now)
  - [ ] Fixed size array based rectangular matrices
  - [ ] Dynamically sized `Vec` based vectors
  - [ ] Dynamically sized `Vec` based square matrices
  - [ ] Dynamically sized `Vec` based rectangular matrices

## TODO next

  - [ ] Add the ability to append two vectors together.
  - [ ] Add the ability to access a matrix block.

# License

This repository is licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or https://www.apache.org/licenses/LICENSE-2.0)
 * MIT License ([LICENSE-MIT](LICENSE-MIT) or https://opensource.org/licenses/MIT)

at your option.
