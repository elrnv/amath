/**
 * This module defines all vector/matrix structures and arithmetic operations on them.
 */

use syn::{self, Token, GenericParam, Generics, Ident, LitInt, Path, Type, TypeParamBound, TypePath};
use syn::token::{Add};
//use syn::synom::Synom;
use syn::parse::{Result, Parse, ParseStream};
use syn::punctuated::Punctuated;
use proc_macro2::{Span, TokenStream};
use quote::{quote, TokenStreamExt};

/**
 * Implement matrix library helpers.
 */

pub struct ScalarImpl {
    pub name: String,
    pub num_types: Punctuated<Type, Token![,]>,
}

impl Parse for ScalarImpl {
    fn parse(input: ParseStream) -> Result<Self> {
        let name = input.parse::<Ident>()?.to_string();
        input.parse::<Token![;]>()?;
        let num_types = Punctuated::parse_separated_nonempty(input)?;
        Ok(ScalarImpl {
            name,
            num_types,
        })
    }
}

pub struct VectorMatrixImpl {
    pub vector_prefix: String,
    pub covector_prefix: String,
    pub matrix_prefix: String,
    pub scalar_trait: Ident,
    pub real_trait: Ident,
    pub from_dim: usize,
    pub to_dim: usize,
    pub num_types: Punctuated<Type, Token![,]>,
}

impl Parse for VectorMatrixImpl {
    fn parse(input: ParseStream) -> Result<Self> {
        let vector_prefix_name: Ident = input.parse()?;
        input.parse::<Token![;]>()?;
        let covector_prefix_name: Ident = input.parse()?;
        input.parse::<Token![;]>()?;
        let matrix_prefix_name: Ident = input.parse()?;
        input.parse::<Token![;]>()?;
        let scalar_trait = input.parse()?;
        input.parse::<Token![;]>()?;
        let real_trait = input.parse()?;
        input.parse::<Token![;]>()?;
        let from_dim_int: LitInt = input.parse()?;
        input.parse::<Token![..=]>()?;
        let to_dim_int: LitInt = input.parse()?;
        input.parse::<Token![;]>()?;
        let num_types = Punctuated::parse_separated_nonempty(input)?;

        Ok(VectorMatrixImpl {
            vector_prefix: vector_prefix_name.to_string(),
            covector_prefix: covector_prefix_name.to_string(),
            matrix_prefix: matrix_prefix_name.to_string(),
            scalar_trait,
            real_trait,
            from_dim: from_dim_int.value() as usize,
            to_dim: to_dim_int.value() as usize,
            num_types
        })
    }
}

/// Helper function to make a vector type name.
pub fn dimensional_type_name(prefix: &String, dim: usize) -> Ident {
    let mut name = prefix.clone();
    name.push_str(&dim.to_string());
    Ident::new(name.as_str(), Span::call_site())
}

pub fn impl_left_scalar_mult(vector: &Ident, num_types: &Punctuated<Type, Token![,]>) -> TokenStream {
    let mut tokens = TokenStream::new();
    for num_type in num_types {
        // loop over each primitive type
        tokens.append_all(quote! {
            // Scalar multiplication
            impl ::std::ops::Mul<#vector<#num_type>> for #num_type {
                type Output = #vector<#num_type>;

                #[inline]
                fn mul(self, rhs: #vector<#num_type>) -> Self::Output {
                    rhs * self
                }
            }

            impl<'a> ::std::ops::Mul<&'a #vector<#num_type>> for #num_type {
                type Output = #vector<#num_type>;

                #[inline]
                fn mul(self, rhs: &'a #vector<#num_type>) -> Self::Output {
                    *rhs * self
                }
            }
        });
    }
    tokens
}

/// Helper routine to implement matrix indexing.
pub fn impl_matrix_index(
    matrix_prefix: &String,
    vector_prefix: &String,
    scalar_trait: &Ident,
    rows: usize,
    cols: usize,
) -> TokenStream {
    let mut tokens = TokenStream::new();

    let matrix = dimensional_type_name(matrix_prefix, cols);
    let vector = dimensional_type_name(vector_prefix, rows);

    // Implement indexing for matrices
    tokens.append_all(quote! {
        // Indexing
        impl<T: #scalar_trait> ::std::ops::Index<usize> for #matrix<T> {
            type Output = #vector<T>;
            #[inline]
            fn index(&self, index: usize) -> &Self::Output {
                unsafe { &*(&self.0[index] as *const [T; #rows] as *const #vector<T>) }
            }
        }
        impl<T: #scalar_trait> ::std::ops::IndexMut<usize> for #matrix<T> {
            #[inline]
            fn index_mut(&mut self, index: usize) -> &mut #vector<T> {
                unsafe { &mut *(&mut self.0[index] as *mut [T; #rows] as *mut #vector<T>) }
            }
        }
    });

    tokens
}

/// Helper routine to implement vector-covector multiply, which produces a rank-one matrix.
pub fn impl_vector_covector_mult(
    vector: &Ident,
    covector: &Ident,
    scalar_trait: &Ident,
    matrix: &Ident,
    cols: usize,
) -> TokenStream {
    let mut tokens = TokenStream::new();
    tokens.append_all(quote! {
        impl<T: #scalar_trait> ::std::ops::Mul<#covector<T>> for #vector<T> {
            type Output = #matrix<T>;
            #[allow(unused_mut)]
            #[inline]
            #[unroll_for_loops]
            fn mul(self, rhs: #covector<T>) -> Self::Output {
                let mut out: #matrix<T> = unsafe { ::std::mem::uninitialized() };
                for col in 0..#cols {
                    out[col] = self * rhs[col];
                }
                out
            }
        }
        // for vector ref
        impl<'a, T: #scalar_trait> ::std::ops::Mul<#covector<T>> for &'a #vector<T> {
            type Output = #matrix<T>;
            #[allow(unused_mut)]
            #[inline]
            #[unroll_for_loops]
            fn mul(self, rhs: #covector<T>) -> Self::Output {
                let mut out: #matrix<T> = unsafe { ::std::mem::uninitialized() };
                for col in 0..#cols {
                    out[col] = self * rhs[col];
                }
                out
            }
        }

        // for covector ref
        impl<'a, T: #scalar_trait> ::std::ops::Mul<&'a #covector<T>> for #vector<T> {
            type Output = #matrix<T>;
            #[allow(unused_mut)]
            #[inline]
            #[unroll_for_loops]
            fn mul(self, rhs: &#covector<T>) -> Self::Output {
                let mut out: #matrix<T> = unsafe { ::std::mem::uninitialized() };
                for col in 0..#cols {
                    out[col] = self * rhs[col];
                }
                out
            }
        }
        // for vector and covector ref
        impl<'a, 'b, T: #scalar_trait> ::std::ops::Mul<&'b #covector<T>> for &'a #vector<T> {
            type Output = #matrix<T>;
            #[allow(unused_mut)]
            #[inline]
            #[unroll_for_loops]
            fn mul(self, rhs: &#covector<T>) -> Self::Output {
                let mut out: #matrix<T> = unsafe { ::std::mem::uninitialized() };
                for col in 0..#cols {
                    out[col] = self * rhs[col];
                }
                out
            }
        }
    });
    tokens
}

/// Helper routine to implement matrix-vector multiply.
pub fn impl_matrix_vector_mult(
    matrix: &Ident,
    scalar_trait: &Ident,
    rhs_type_name: &Ident,
    out_type_name: &Ident,
    cols: usize,
) -> TokenStream {
    let mut tokens = TokenStream::new();
    tokens.append_all(quote! {
        impl<T: #scalar_trait> ::std::ops::Mul<#rhs_type_name<T>> for #matrix<T> {
            type Output = #out_type_name<T>;
            #[allow(unused_mut)]
            #[inline]
            #[unroll_for_loops]
            fn mul(self, rhs: #rhs_type_name<T>) -> Self::Output {
                let mut out = self[0] * rhs[0];
                for i in 1..#cols { out += self[i] * rhs[i]; }
                out
            }
        }
        // for matrix ref
        impl<'a, T: #scalar_trait> ::std::ops::Mul<#rhs_type_name<T>> for &'a #matrix<T> {
            type Output = #out_type_name<T>;
            #[allow(unused_mut)]
            #[inline]
            #[unroll_for_loops]
            fn mul(self, rhs: #rhs_type_name<T>) -> Self::Output {
                let mut out = self[0] * rhs[0];
                for i in 1..#cols { out += self[i] * rhs[i]; }
                out
            }
        }

        // for vector ref
        impl<'a, T: #scalar_trait> ::std::ops::Mul<&'a #rhs_type_name<T>> for #matrix<T> {
            type Output = #out_type_name<T>;
            #[allow(unused_mut)]
            #[inline]
            #[unroll_for_loops]
            fn mul(self, rhs: &#rhs_type_name<T>) -> Self::Output {
                let mut out = self[0] * rhs[0];
                for i in 1..#cols { out += self[i] * rhs[i]; }
                out
            }
        }
        // for vector and matrix ref
        impl<'a, 'b, T: #scalar_trait> ::std::ops::Mul<&'b #rhs_type_name<T>> for &'a #matrix<T> {
            type Output = #out_type_name<T>;
            #[allow(unused_mut)]
            #[inline]
            #[unroll_for_loops]
            fn mul(self, rhs: &#rhs_type_name<T>) -> Self::Output {
                let mut out = self[0] * rhs[0];
                for i in 1..#cols { out += self[i] * rhs[i]; }
                out
            }
        }
    });
    tokens
}

/// Helper routine to implement covector-matrix multiply.
pub fn impl_covector_matrix_mult(
    matrix: &Ident,
    scalar_trait: &Ident,
    covector: &Ident,
    cols: usize,
) -> TokenStream {
    let mut tokens = TokenStream::new();
    tokens.append_all(quote! {
        impl<T: #scalar_trait> ::std::ops::Mul<#matrix<T>> for #covector<T> {
            type Output = #covector<T>;
            #[allow(unused_mut)]
            #[inline]
            #[unroll_for_loops]
            fn mul(self, rhs: #matrix<T>) -> Self::Output {
                let mut out: #covector<T> = unsafe { ::std::mem::uninitialized() };
                for i in 0..#cols { out[i] = self * rhs[i]; }
                out
            }
        }
        // for matrix ref
        impl<'a, T: #scalar_trait> ::std::ops::Mul<#matrix<T>> for &'a #covector<T> {
            type Output = #covector<T>;
            #[allow(unused_mut)]
            #[inline]
            #[unroll_for_loops]
            fn mul(self, rhs: #matrix<T>) -> Self::Output {
                let mut out: #covector<T> = unsafe { ::std::mem::uninitialized() };
                for i in 0..#cols { out[i] = self * rhs[i]; }
                out
            }
        }

        // for covector ref
        impl<'a, T: #scalar_trait> ::std::ops::Mul<&'a #matrix<T>> for #covector<T> {
            type Output = #covector<T>;
            #[allow(unused_mut)]
            #[inline]
            #[unroll_for_loops]
            fn mul(self, rhs: &#matrix<T>) -> Self::Output {
                let mut out: #covector<T> = unsafe { ::std::mem::uninitialized() };
                for i in 0..#cols { out[i] = self * rhs[i]; }
                out
            }
        }
        // for covector and matrix ref
        impl<'a, 'b, T: #scalar_trait> ::std::ops::Mul<&'b #matrix<T>> for &'a #covector<T> {
            type Output = #covector<T>;
            #[allow(unused_mut)]
            #[inline]
            #[unroll_for_loops]
            fn mul(self, rhs: &#matrix<T>) -> Self::Output {
                let mut out: #covector<T> = unsafe { ::std::mem::uninitialized() };
                for i in 0..#cols { out[i] = self * rhs[i]; }
                out
            }
        }
    });
    tokens
}

/// Helper routine to implement matrix-matrix multiply.
pub fn impl_matrix_matrix_mult(matrix: &Ident, scalar_trait: &Ident, cols: usize) -> TokenStream {
    let mut tokens = TokenStream::new();
    tokens.append_all(quote! {
        impl<T: #scalar_trait> ::std::ops::Mul<#matrix<T>> for #matrix<T> {
            type Output = #matrix<T>;
            #[inline]
            #[unroll_for_loops]
            fn mul(self, rhs: #matrix<T>) -> Self::Output {
                let mut out: #matrix<T> = unsafe { ::std::mem::uninitialized() };
                for i in 0..#cols {
                    out[i] = self * rhs[i];
                }
                out
            }
        }
        impl<T: #scalar_trait> ::std::ops::MulAssign<#matrix<T>> for #matrix<T> {
            #[inline]
            fn mul_assign(&mut self, rhs: #matrix<T>) {
                *self = *self * rhs;
            }
        }
        // references
        impl<'a, 'b, T: #scalar_trait> ::std::ops::Mul<&'a #matrix<T>> for &'b #matrix<T> {
            type Output = #matrix<T>;
            #[inline]
            #[unroll_for_loops]
            fn mul(self, rhs: &#matrix<T>) -> Self::Output {
                let mut out: #matrix<T> = unsafe { ::std::mem::uninitialized() };
                for i in 0..#cols {
                    out[i] += self * rhs[i];
                }
                out
            }
        }
        impl<'a, T: #scalar_trait> ::std::ops::MulAssign<&'a #matrix<T>> for #matrix<T> {
            #[inline]
            fn mul_assign(&mut self, rhs: &#matrix<T>) {
                *self = &*self * rhs;
            }
        }

        // Multiplicative identity
        impl<T: #scalar_trait> ::num_traits::identities::One for #matrix<T> {
            #[inline]
            fn one() -> Self {
                #matrix::identity()
            }
        }
    });
    tokens
}

/// Helper routine to implement operations specific to `n`x`n` square matrices.
pub fn impl_square_matrix(
    matrix_prefix: &String,
    _vector_prefix: &String,
    scalar_trait: &Ident,
    n: usize,
) -> TokenStream {
    let mut tokens = TokenStream::new();

    let matrix = dimensional_type_name(matrix_prefix, n);
    //let vector = dimensional_type_name(vector_prefix, n);

    // Implement transpose
    tokens.append_all(quote! {
        impl<T: #scalar_trait> #matrix<T> {
            /// Assign a row of a matrix from a given array.
            #[inline]
            #[unroll_for_loops]
            pub fn assign_row(&mut self, row: usize, vec: &[T;#n]) {
                for i in 0..#n {
                    self[i][row] = vec[i];
                }
            }
            /// Take the transpose of the given matrix.
            #[inline]
            #[unroll_for_loops]
            pub fn transpose(&self) -> #matrix<T> {
                let mut m: #matrix<T> = unsafe {
                    ::std::mem::uninitialized()
                };
                for col in 0..#n {
                    m.assign_row(col, &self[col].0);
                }
                m
            }
        }
    });

    let n_minus_one = n - 1;
    let smaller_matrix = dimensional_type_name(matrix_prefix, n_minus_one);
    // TODO: Implement exact determinant with exact geometry predicates.
    if n < 2 {
        // Base case
        tokens.append_all(quote! {
            impl<T: #scalar_trait> #matrix<T> {
                /// Compute the determinant of a 1x1 matrix.
                #[inline]
                pub fn determinant(&self) -> T {
                    self[0][0]
                }
            }
        });
    } else {
        tokens.append_all(quote! {
            impl<T: #scalar_trait> #matrix<T> {
                /// Construct a matrix smaller in both dimensions by 1 that is the same as the
                /// original matrix but without the first row and a given column. Although this is
                /// a very specific function, it is useful for efficient co-factor expansions.
                #[inline]
                #[unroll_for_loops]
                pub fn without_col_and_first_row(&self, col: usize) -> #smaller_matrix<T> {
                    let mut m: [[T; #n_minus_one]; #n_minus_one] = unsafe {
                        ::std::mem::uninitialized()
                    };
                    for i in 0..#n_minus_one {
                        m[i].copy_from_slice(&self[if i < col { i } else { i + 1 }].0[1..#n]);
                    }
                    #smaller_matrix(m)
                }

                /// Compute the determinant of the matrix recursively.
                #[inline]
                #[unroll_for_loops]
                pub fn determinant(&self) -> T {
                    let mut det = self[0][0]*self.without_col_and_first_row(0).determinant();
                    for col in 1..#n {
                        let cofactor =
                            self[col][0]*self.without_col_and_first_row(col).determinant();
                        if col & 1 == 0 {
                            det += cofactor;
                        } else {
                            det -= cofactor;
                        }
                    }
                    det
                }
            }
        });
    }
    match n {
        1 => tokens.append_all(quote! {
            impl<T: #scalar_trait> #matrix<T> {
                /// Compute the inverse of a 1x1 matrix.
                #[inline]
                pub fn inverse(&self) -> Option<#matrix<T>> {
                    let denom = self[0][0];
                    if denom != T::zero() {
                        Some(#matrix([[T::one()/denom]]))
                    } else {
                        None
                    }
                }
                /// Invert the 1x1 matrix in place. Return true if inversion was successful.
                #[inline]
                pub fn invert(&mut self) -> bool {
                    let denom = self[0][0];
                    if denom != T::zero() {
                        self[0][0] = T::one()/denom;
                        true
                    } else {
                        false
                    }
                }
            }
        }),
        2 => tokens.append_all(quote! {
            impl<T: #scalar_trait + num_traits::Float> #matrix<T> {
                /// Compute the inverse of a 2x2 matrix.
                #[inline]
                pub fn inverse(&self) -> Option<#matrix<T>> {
                    let det = self.determinant();
                    if det != T::zero() {
                        Some(#matrix([[self[1][1] / det, -self[0][1] / det],
                                     [-self[1][0] / det, self[0][0] / det]]))
                    } else {
                        None
                    }
                }
                /// Compute the transpose of a 3x3 matrix inverse.
                #[inline]
                pub fn inverse_transpose(&self) -> Option<#matrix<T>> {
                    let det = self.determinant();
                    if det != T::zero() {
                        Some(#matrix([[self[1][1] / det, -self[1][0] / det],
                                     [-self[0][1] / det, self[0][0] / det]]))
                    } else {
                        None
                    }
                }
                /// Invert the 2x2 matrix in place. Return true if inversion was successful.
                #[inline]
                pub fn invert(&mut self) -> bool {
                    let det = self.determinant();
                    if det != T::zero() {
                        {
                            let (a,b) = self.0.split_at_mut(1);
                            ::std::mem::swap(&mut a[0][0], &mut b[0][1]);
                        }
                        self[0][1] = -self[0][1];
                        self[1][0] = -self[1][0];
                        *self /= det;
                        true
                    } else {
                        false
                    }
                }
            }
        }),
        3 => tokens.append_all(quote! {
            impl<T: #scalar_trait + num_traits::Float> #matrix<T> {
                /// Compute the inverse of a 3x3 matrix.
                #[inline]
                pub fn inverse(&self) -> Option<#matrix<T>> {
                    self.inverse_transpose().map(|x| x.transpose())
                }
                /// Compute the transpose of a 3x3 matrix inverse.
                #[inline]
                pub fn inverse_transpose(&self) -> Option<#matrix<T>> {
                    let det = self.determinant();
                    if det != T::zero() {
                        Some(#matrix([(self[1].cross(self[2]) / det).into(),
                                      (self[2].cross(self[0]) / det).into(),
                                      (self[0].cross(self[1]) / det).into()]))
                    } else {
                        None
                    }
                }
                /// Invert the 3x3 matrix in place. Return true if inversion was successful.
                #[inline]
                pub fn invert(&mut self) -> bool {
                    match self.inverse() {
                        Some(inv) => { *self = inv; true },
                        None => false,
                    }
                }
            }
        }),
        _ => {}
    };
    tokens
}

/// Helper routine to implement matrix vectorization.
pub fn impl_vectorize_matrix(
    matrix_prefix: &String,
    vector_prefix: &String,
    scalar_trait: &Ident,
    rows: usize,
    cols: usize,
    limit_n: usize,
) -> TokenStream {
    let mut tokens = TokenStream::new();

    let vec_size = cols * rows;
    let matrix = dimensional_type_name(matrix_prefix, cols);
    let vector = dimensional_type_name(vector_prefix, vec_size);

    if vec_size <= limit_n {
        tokens.append_all(quote! {
            impl<T: #scalar_trait> #matrix<T> {
                #[inline]
                #[unroll_for_loops]
                pub fn vec(&self) -> #vector<T> {
                    let mut vec: [T; #vec_size] = unsafe { ::std::mem::uninitialized() };
                    for col in 0..#cols {
                        for row in 0..#rows {
                            vec[#cols*col + row] = self[col][row];
                        }
                    }
                    #vector(vec)
                }
                #[inline]
                pub fn vec_ref(&self) -> &#vector<T> {
                    unsafe { &*(self as *const #matrix<T> as *const #vector<T>) }
                }
            }
        });
    } // TODO: else return a dynamically sized vector.
    tokens
}

/// Helper routine to implement matrix vectorization.
pub fn impl_norm(name: &Ident, scalar_trait: &Ident) -> TokenStream {
    quote! {
        impl<T: #scalar_trait> #name<T> {
            /// Frobenius norm squared: the sum of squares of all elements of the matrix.
            #[inline]
            pub fn norm_squared(&self) -> T {
                (*self).map(|x| x*x).sum()
            }
        }

        impl<T: #scalar_trait + ::num_traits::Float> #name<T> {
            /// Frobenius norm: the square root of the sum of squares of all elements of the matrix.
            #[inline]
            pub fn norm(&self) -> T {
                self.norm_squared().sqrt()
            }
        }

        impl<T: #scalar_trait + ::num_traits::cast::ToPrimitive> #name<T> {
            /// Frobenius norm: the square root of the sum of squares of all elements of the matrix.
            /// This function casts all numeric types to f64 for a more precise square root
            /// computation.
            #[inline]
            pub fn norm_f64(&self) -> f64 {
                // As of this writing, everything casts to f64 successfully.
                self.norm_squared().to_f64().unwrap().sqrt()
            }
        }
    }
}

pub fn impl_col_major_square_matrices(
    matrix_prefix: &String,
    vector_prefix: &String,
    scalar_trait: &Ident,
    n: usize,
    limit_n: usize,
) -> TokenStream {
    let mut tokens = TokenStream::new();

    let matrix = dimensional_type_name(&matrix_prefix, n);
    //let vector = dimensional_type_name(&vector_prefix, n);
    let rows = n;
    let cols = n;

    tokens.append_all(quote! {
        #[derive(Copy, Clone, Debug, PartialEq, PartialOrd, Matrix)]
        #[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
        pub struct #matrix<T: #scalar_trait>(pub [[T; #rows]; #cols]);
    });

    tokens.append_all(impl_matrix_index(
        &matrix_prefix,
        &vector_prefix,
        &scalar_trait,
        rows,
        cols,
    ));
    tokens.append_all(impl_matrix_matrix_mult(&matrix, &scalar_trait, cols));
    tokens.append_all(impl_square_matrix(
        &matrix_prefix,
        &vector_prefix,
        &scalar_trait,
        cols,
    ));
    tokens.append_all(impl_vectorize_matrix(
        &matrix_prefix,
        &vector_prefix,
        &scalar_trait,
        rows,
        cols,
        limit_n,
    ));
    tokens.append_all(impl_norm(&matrix, scalar_trait));

    tokens
}

pub fn impl_vector(
    orig_generics: &Generics,
    array_type: &Type,
    n: &LitInt,
    vector: &Ident,
    scalar: &Type,
) -> TokenStream {
    let scalar_traits = get_scalar_traits(orig_generics, scalar);

    let (impl_generics, ty_generics, where_clause) = orig_generics.split_for_impl();
    let mut tokens = TokenStream::new();
    tokens.append_all(quote! {
        impl #impl_generics #vector #ty_generics #where_clause {
            // Constructors
            #[inline]
            pub fn ones() -> Self {
                #vector([#scalar::one(); #n])
            }
            #[inline]
            pub fn zeros() -> Self {
                #vector([#scalar::zero(); #n])
            }
            #[inline]
            pub fn size() -> usize {
                #n
            }
            #[inline]
            pub fn unit(axis: usize) -> Self {
                let mut zeros = Self::zeros();
                zeros[axis] = #scalar::one();
                zeros
            }

            /// Apply a function to each element in the vector. This function returns a mut
            /// reference to the modified vector.
            #[inline]
            #[unroll_for_loops]
            pub fn apply<F_>(&mut self, mut f: F_) -> &mut Self
                where F_: FnMut(#scalar) -> #scalar, {
                    for i in 0..#n {
                        self[i] = f(self[i]);
                    }
                    self
                }

            /// Map a function over each element in the vector. This function returns a new vector.
            #[inline]
            #[unroll_for_loops]
            pub fn map<U_: #scalar_traits, F_>(&self, mut f: F_) -> #vector<U_>
                where F_: FnMut(#scalar) -> U_,
            {
                let mut out = [unsafe { std::mem::uninitialized() }; #n];
                for i in 0..#n {
                    out[i] = f(self[i]);
                }
                #vector(out)
            }

            #[inline]
            #[unroll_for_loops]
            pub fn fold<B_, F_>(&self, mut init: B_, mut f: F_) -> B_
                where B_: #scalar_traits,
                      F_: FnMut(B_, #scalar) -> B_,
            {
                for i in 0..#n {
                    init = f(init, self[i]);
                }
                init
            }

            #[inline]
            pub fn sum(&self) -> #scalar {
                self.fold(#scalar::zero(), |acc, x| x + acc)
            }

            #[allow(unused_mut)]
            #[inline]
            #[unroll_for_loops]
            pub fn dot(&self, other: Self) -> #scalar {
                let mut prod = self[0]*other[0];
                for i in 1..#n {
                    prod += self[i]*other[i];
                }
                prod
            }

            #[inline]
            pub fn into_inner(self) -> #array_type {
                self.0
            }
        }

        impl<T: #scalar_traits + ::num_traits::cast::ToPrimitive> #vector <T> {
            #[inline]
            #[unroll_for_loops]
            pub fn cast<U: #scalar_traits + ::num_traits::cast::NumCast>(&self) -> Option<#vector <U>> {
                let mut casted: #vector <U>;
                casted = unsafe { ::std::mem::uninitialized() };
                for i in 0..#n {
                    casted[i] = U::from(self[i])?;
                }
                Some(casted)
            }
        }

        // Conversion
        impl #impl_generics From<#array_type> for #vector #ty_generics #where_clause {
            #[inline]
            fn from(array: #array_type) -> Self {
                #vector(array)
            }
        }

        impl #impl_generics Into<#array_type> for #vector #ty_generics #where_clause {
            #[inline]
            fn into(self) -> #array_type {
                self.0
            }
        }

        // Indexing
        impl #impl_generics ::std::ops::Index<usize> for #vector #ty_generics #where_clause {
            type Output = #scalar;
            #[inline]
            fn index(&self, index: usize) -> &Self::Output {
                &self.0[index]
            }
        }
        impl #impl_generics ::std::ops::IndexMut<usize> for #vector #ty_generics #where_clause {
            #[inline]
            fn index_mut(&mut self, index: usize) -> &mut #scalar {
                &mut self.0[index]
            }
        }

        // Addition
        impl #impl_generics ::std::ops::Add for #vector #ty_generics #where_clause {
            type Output = Self;
            #[inline]
            fn add(mut self, rhs: Self) -> Self::Output {
                self += rhs;
                self
            }
        }
        impl #impl_generics ::std::ops::AddAssign for #vector #ty_generics #where_clause {
            #[inline]
            #[unroll_for_loops]
            fn add_assign(&mut self, rhs: Self) {
                for i in 0..#n {
                    self.0[i] += rhs.0[i];
                }
            }
        }

        // Subtraction
        impl #impl_generics ::std::ops::Sub for #vector #ty_generics #where_clause {
            type Output = Self;
            #[inline]
            fn sub(mut self, rhs: Self) -> Self::Output {
                self -= rhs;
                self
            }
        }
        impl #impl_generics ::std::ops::SubAssign for #vector #ty_generics #where_clause {
            #[inline]
            #[unroll_for_loops]
            fn sub_assign(&mut self, rhs: Self) {
                for i in 0..#n {
                    self.0[i] -= rhs.0[i];
                }
            }
        }

        // Additive identity
        impl #impl_generics ::num_traits::identities::Zero for #vector #ty_generics #where_clause {
            #[inline]
            fn zero() -> Self {
                #vector::zeros()
            }
            #[inline]
            fn is_zero(&self) -> bool {
                self.0 == [#scalar::zero(); #n]
            }
        }

        // Sum
        impl #impl_generics std::iter::Sum for #vector #ty_generics #where_clause {
            fn sum<I: Iterator<Item=#vector<T>>>(iter: I) -> Self {
                iter.fold(Self::zeros(), |a, b| a + b)
            }
        }

        // Sum over references
        impl<'a, T: #scalar_traits> std::iter::Sum<&'a #vector<T>> for #vector #ty_generics #where_clause {
            fn sum<I: Iterator<Item=&'a Self>>(iter: I) -> Self {
                iter.fold(#vector::zeros(), |a, &b| a + b)
            }
        }

        // Implement Default for convenience
        impl #impl_generics Default for #vector #ty_generics #where_clause {
            #[inline]
            fn default() -> Self {
                #vector::zeros()
            }
        }
    });

    // Implement list style accessors.
    if n.value() > 0 {
        let n_minus_one = LitInt::new(n.value() - 1, n.suffix().clone(), n.span());
        tokens.append_all(quote! {
            impl #impl_generics #vector #ty_generics #where_clause {
                /// First element in the vector. Same as indexing at zero.
                #[inline]
                pub fn head(&self) -> #scalar {
                    self.0[0]
                }
                /// A view of all but the first element of the vector. This is useful for
                /// implementing determinant computation using cofactor expansion.
                #[inline]
                pub fn tail(&self) -> [#scalar; #n_minus_one] {
                    unsafe {
                        let mut t: [#scalar; #n_minus_one] = ::std::mem::uninitialized();
                        t.copy_from_slice(&self.0[1..#n]);
                        t
                    }
                }
                #[inline]
                pub fn tail_slice(&self) -> &[#scalar] {
                    &self.0[1..#n]
                }
            }
        });
    }

    // Implement specialized operations on 2D and 3D vectors
    if n.value() == 2 {

    } else if n.value() == 3 {
        tokens.append_all(quote! {
            impl #impl_generics #vector #ty_generics #where_clause {
                /// Cross product with other 3D vectors
                #[inline]
                pub fn cross(self, other: #vector #ty_generics) -> #vector #ty_generics {
                    #vector([self[1]*other[2] - self[2]*other[1],
                             self[2]*other[0] - self[0]*other[2],
                             self[0]*other[1] - self[1]*other[0]])
                }
            }
        });
    }

    tokens.append_all(impl_scalar_ops(orig_generics, n, vector, scalar));

    let mut generics = (*orig_generics).clone();
    // Add a generic parameter needed for reference impls. We only want to change impl generics.
    generics.params.insert(
        0,
        GenericParam::Lifetime(syn::parse(quote!('a).into()).unwrap()),
    );
    let (impl_generics, _, _) = generics.split_for_impl();

    tokens.append_all(quote! {
        // This isn't part of the derive because it requires an extra lifetime parameter, which
        // is easier to add here. Consider it a non-standard conversion.
        impl #impl_generics From<&'a [T]> for #vector #ty_generics #where_clause {
            /// Convert a slice to a vector. This conversion panics if the slice is smaller
            /// than the vector.
            #[inline]
            #[unroll_for_loops]
            fn from(slice: &'a [T]) -> Self {
                let mut arr: [T; #n] = unsafe { ::std::mem::uninitialized() };
                for i in 0..#n { // the #n here ensures that everything will be initialized.
                    arr[i] = slice[i];
                }
                #vector(arr)
            }
        }
    });
    tokens
}

/// Helper function that looks for the traits associated with the given "scalar" type. Since `Type`
/// can technically be an actual type as opposed to a generic type, this may fail, in which case we
/// will just return the `Any` trait for simplicity.
fn get_scalar_traits(generics: &Generics, scalar: &Type) -> Punctuated<TypeParamBound, Add> {
    // Determine what was the trait bounds on the scalar type if any. If we can't find it, just
    // impose a dummy "Any" trait to make the implementation simpler.
    let mut scalar_traits: Punctuated<TypeParamBound, Add> = Punctuated::new();
    scalar_traits.push(syn::parse(quote!(Any).into()).unwrap());
    if let Type::Path(TypePath {
        path: Path { ref segments, .. },
        ..
    }) = *scalar
    {
        let scalar_ident = &segments.last().unwrap().value().ident;
        match generics.type_params().find(|x| x.ident == *scalar_ident) {
            Some(type_param) => {
                scalar_traits = type_param.bounds.clone();
            }
            None => {}
        }
    }
    scalar_traits
}

pub fn impl_matrix(
    generics: &Generics,
    array_type: &Type,
    rows: &LitInt,
    cols: &LitInt,
    matrix: &Ident,
    scalar: &Type,
) -> TokenStream {
    let scalar_traits = get_scalar_traits(generics, scalar);

    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    let mut tokens = TokenStream::new();
    let min_rows_cols = rows.value().min(cols.value()) as usize;
    tokens.append_all(quote! {
        impl #impl_generics #matrix #ty_generics #where_clause {
            // Constructors
            #[inline]
            pub fn ones() -> Self {
                #matrix([[#scalar::one(); #rows]; #cols])
            }
            #[inline]
            pub fn zeros() -> Self {
                #matrix([[#scalar::zero(); #rows]; #cols])
            }
            /// Matrix identity. If this matrix is not square, then this function simply puts
            /// ones on the diagonal and zeros everywhere else.
            #[inline]
            #[unroll_for_loops]
            pub fn identity() -> Self {
                Self::diag([#scalar::one(); #min_rows_cols])
            }

            /// Construct a diagonal matrix from a given array of diagonal entries.
            #[inline]
            #[unroll_for_loops]
            pub fn diag(diag: [#scalar; #min_rows_cols]) -> Self {
                let mut zeros = Self::zeros();
                for i in 0..#min_rows_cols {
                    zeros[i][i] = diag[i];
                }
                zeros
            }

            // Info
            #[inline]
            pub fn rows() -> usize {
                #rows
            }
            #[inline]
            pub fn cols() -> usize {
                #cols
            }
            #[inline]
            pub fn size() -> (usize, usize) {
                (Self::rows(), Self::cols())
            }

            /// Apply a function to each element in the matrix. This function returns a mut
            /// reference to the modified matrix.
            #[inline]
            #[unroll_for_loops]
            pub fn apply<F_>(&mut self, mut f: F_) -> &mut Self
                where F_: FnMut(#scalar) -> #scalar, {
                for col in 0..#cols {
                    self[col].apply(|x| f(x));
                }
                self
            }

            /// Map a function over each element in the matrix. This function consumes the given
            /// matrix and returns a new matrix.
            #[inline]
            #[unroll_for_loops]
            pub fn map<U_: #scalar_traits, F_>(&self, mut f: F_) -> #matrix<U_>
                where F_: FnMut(#scalar) -> U_,
            {
                let mut out = [unsafe { std::mem::uninitialized() }; #cols];
                for col in 0..#cols {
                    out[col] = self[col].map(|x| f(x)).into();
                }
                #matrix(out)
            }

            #[inline]
            #[unroll_for_loops]
            pub fn fold<B_, F_>(&self, mut init: B_, mut f: F_) -> B_
                where B_: #scalar_traits,
                      F_: FnMut(B_, #scalar) -> B_, {
                for i in 0..#cols {
                    init = self[i].fold(init, |acc, x| f(acc, x));
                }
                init
            }

            /// Sum all the elements in the matrix and return the result.
            #[inline]
            pub fn sum(&self) -> #scalar {
                self.fold(#scalar::zero(), |acc, x| x + acc)
            }

            #[allow(unused_mut)]
            #[inline]
            #[unroll_for_loops]
            pub fn trace(&self) -> #scalar {
                let mut tr = self[0][0];
                for i in 1..#min_rows_cols {
                    tr += self[i][i];
                }
                tr
            }

            #[inline]
            pub fn into_inner(self) -> #array_type {
                self.0
            }
        }

        // Conversion
        impl #impl_generics From<#array_type> for #matrix #ty_generics #where_clause {
            #[inline]
            fn from(array: #array_type) -> Self {
                #matrix(array)
            }
        }

        impl #impl_generics Into<#array_type> for #matrix #ty_generics #where_clause {
            #[inline]
            fn into(self) -> #array_type {
                self.0
            }
        }

        // Addition
        impl #impl_generics ::std::ops::Add for #matrix #ty_generics #where_clause {
            type Output = Self;
            #[inline]
            fn add(mut self, rhs: Self) -> Self::Output {
                self += rhs;
                self
            }
        }
        impl #impl_generics ::std::ops::AddAssign for #matrix #ty_generics #where_clause {
            #[inline]
            #[unroll_for_loops]
            fn add_assign(&mut self, rhs: Self) {
                for i in 0..#cols {
                    self[i] += rhs[i];
                }
            }
        }

        // Subtraction
        impl #impl_generics ::std::ops::Sub for #matrix #ty_generics #where_clause {
            type Output = Self;
            #[inline]
            fn sub(mut self, rhs: Self) -> Self::Output {
                self -= rhs;
                self
            }
        }
        impl #impl_generics ::std::ops::SubAssign for #matrix #ty_generics #where_clause {
            #[inline]
            #[unroll_for_loops]
            fn sub_assign(&mut self, rhs: Self) {
                for i in 0..#cols {
                    self[i] -= rhs[i];
                }
            }
        }

        // Sum
        impl #impl_generics std::iter::Sum for #matrix #ty_generics #where_clause {
            fn sum<I: Iterator<Item=#matrix<T>>>(iter: I) -> Self {
                iter.fold(Self::zeros(), |a, b| a + b)
            }
        }

        // Sum over references
        impl<'a, T: #scalar_traits> std::iter::Sum<&'a #matrix<T>> for #matrix #ty_generics #where_clause {
            fn sum<I: Iterator<Item=&'a Self>>(iter: I) -> Self {
                iter.fold(Self::zeros(), |a, &b| a + b)
            }
        }

        // Additive identity
        impl #impl_generics ::num_traits::identities::Zero for #matrix #ty_generics #where_clause {
            #[inline]
            fn zero() -> Self {
                #matrix::zeros()
            }
            #[inline]
            fn is_zero(&self) -> bool {
                self.0 == [[#scalar::zero(); #rows]; #cols]
            }
        }

        // Implement Default for convenience
        impl #impl_generics Default for #matrix #ty_generics #where_clause {
            #[inline]
            fn default() -> Self {
                #matrix::zeros()
            }
        }

    });
    tokens.append_all(impl_scalar_ops(generics, cols, matrix, scalar));
    tokens
}

fn impl_scalar_ops(
    orig_generics: &Generics,
    n: &LitInt,
    vector: &Ident,
    scalar: &Type,
) -> TokenStream {
    let mut tokens = TokenStream::new();
    let (impl_generics, ty_generics, where_clause) = orig_generics.split_for_impl();
    tokens.append_all(quote! {
        // Scalar Multiplication
        impl #impl_generics ::std::ops::Mul<#scalar> for #vector #ty_generics #where_clause {
            type Output = Self;
            #[inline]
            fn mul(mut self, rhs: #scalar) -> Self::Output {
                self *= rhs;
                self
            }
        }
        // ... with assign
        impl #impl_generics ::std::ops::MulAssign<#scalar> for #vector #ty_generics #where_clause {
            #[inline]
            #[unroll_for_loops]
            fn mul_assign(&mut self, rhs: #scalar) {
                for i in 0..#n {
                    self[i] *= rhs;
                }
            }
        }

        // Scalar Division
        impl #impl_generics ::std::ops::Div<#scalar> for #vector #ty_generics #where_clause {
            type Output = Self;
            #[inline]
            fn div(mut self, rhs: #scalar) -> Self::Output {
                self /= rhs;
                self
            }
        }
        // ... with assign
        impl #impl_generics ::std::ops::DivAssign<#scalar> for #vector #ty_generics #where_clause {
            #[inline]
            #[unroll_for_loops]
            fn div_assign(&mut self, rhs: #scalar) {
                for i in 0..#n {
                    self[i] /= rhs;
                }
            }
        }

        // Sacalar Remainder
        impl #impl_generics ::std::ops::Rem<#scalar> for #vector #ty_generics #where_clause {
            type Output = Self;
            #[inline]
            fn rem(mut self, rhs: #scalar) -> Self::Output {
                self %= rhs;
                self
            }
        }
        // ... with assign
        impl #impl_generics ::std::ops::RemAssign<#scalar> for #vector #ty_generics #where_clause {
            #[inline]
            #[unroll_for_loops]
            fn rem_assign(&mut self, rhs: #scalar) {
                for i in 0..#n {
                    self[i] %= rhs;
                }
            }
        }
    });

    let mut generics = (*orig_generics).clone();
    // Add a generic parameter needed for reference impls. We only want to change impl generics.
    generics.params.insert(
        0,
        GenericParam::Lifetime(syn::parse(quote!('a).into()).unwrap()),
    );
    let (impl_generics, _, _) = generics.split_for_impl();

    tokens.append_all(quote! {
        // Scalar Multiplication by reference
        impl #impl_generics ::std::ops::Mul<#scalar> for &'a #vector #ty_generics #where_clause {
            type Output = #vector #ty_generics;
            #[inline]
            fn mul(self, rhs: #scalar) -> Self::Output {
                let mut out = *self;
                out *= rhs;
                out
            }
        }

        // Scalar Division by reference
        impl #impl_generics ::std::ops::Div<#scalar> for &'a #vector #ty_generics #where_clause {
            type Output = #vector #ty_generics;
            #[inline]
            fn div(self, rhs: #scalar) -> Self::Output {
                let mut out = *self;
                out /= rhs;
                out
            }
        }

        // Scalar Remainder by reference
        impl #impl_generics ::std::ops::Rem<#scalar> for &'a #vector #ty_generics #where_clause {
            type Output = #vector #ty_generics;
            #[inline]
            fn rem(self, rhs: #scalar) -> Self::Output {
                let mut out = *self;
                out %= rhs;
                out
            }
        }
    });

    // Try to add the ::std::ops::Neg trait to the list of scalar traits in order to implement Neg.
    let mut generics = (*orig_generics).clone();

    let add_trait_successful = if let Type::Path(TypePath {
        path: Path { ref segments, .. },
        ..
    }) = *scalar
    {
        let scalar_ident = &segments.last().unwrap().value().ident;
        match generics.type_params_mut().find(|x| x.ident == *scalar_ident) {
            Some(type_param) => {
                type_param
                    .bounds
                    .push(syn::parse(quote!(::std::ops::Neg<Output=#scalar>).into()).unwrap());
                true
            }
            None => false,
        }
    } else {
        false
    };

    if add_trait_successful {
        let mut toks = {
            let (impl_generics, _, _) = generics.split_for_impl();

            quote! {
                // Negation
                impl #impl_generics ::std::ops::Neg for #vector #ty_generics #where_clause {
                    type Output = #vector #ty_generics;
                    #[inline]
                    #[unroll_for_loops]
                    fn neg(mut self) -> Self::Output {
                        for i in 0..#n {
                            self[i] = -self[i];
                        }
                        self
                    }
                }
            }
        };

        // Add a generic parameter needed for reference impl. We only want to change impl generics.
        generics.params.insert(
            0,
            GenericParam::Lifetime(syn::parse(quote!('a).into()).unwrap()),
        );
        let (impl_generics, _, _) = generics.split_for_impl();

        toks.append_all(quote! {
            impl #impl_generics ::std::ops::Neg for &'a #vector #ty_generics #where_clause {
                type Output = #vector #ty_generics;
                #[inline]
                #[unroll_for_loops]
                fn neg(self) -> Self::Output {
                    let mut out = *self;
                    for i in 0..#n {
                        out[i] = -out[i];
                    }
                    out
                }
            }
        });
        tokens.append_all(toks);
    }
    tokens
}
