#![recursion_limit = "256"]

/**
 * This module is the entry point for procedural macros used in the main `amath` crate. It defines
 * the necessary macros and calls their implementations in `math.rs`.
 */

extern crate proc_macro;

mod math;

use syn::{self, Data, DataStruct, DeriveInput, Expr, ExprLit, Fields,
          Lit, Type, TypeArray,
          };
use proc_macro2::TokenStream;
use quote::{quote, TokenStreamExt};

#[proc_macro_derive(Vector)]
pub fn vector(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input: DeriveInput = syn::parse(input).unwrap();

    let name = &input.ident;

    if let Data::Struct(DataStruct {
        fields: Fields::Unnamed(ref fields),
        ..
    }) = input.data
    {
        if let Some(field_pair) = fields.unnamed.first() {
            let field_type = &field_pair.value().ty;
            if let Type::Array(TypeArray {
                elem: ref scalar,
                len:
                    Expr::Lit(ExprLit {
                        lit: Lit::Int(ref n),
                        ..
                    }),
                ..
            }) = *field_type
            {
                math::impl_vector(&input.generics, field_type, n, name, scalar).into()
            } else {
                panic!("Only arrays are allowed to implement Vectors.");
            }
        } else {
            panic!("Only newtypes are allowed to be Vectors.");
        }
    } else {
        panic!("Vector wrappers can wrap only one array type at a time.");
    }
}

// Column matrix
#[proc_macro_derive(Matrix)]
pub fn matrix(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input: DeriveInput = syn::parse(input).unwrap();

    let name = &input.ident;

    if let Data::Struct(DataStruct {
        fields: Fields::Unnamed(ref fields),
        ..
    }) = input.data
    {
        if let Some(field_pair) = fields.unnamed.first() {
            let field_type = &field_pair.value().ty;
            if let Type::Array(TypeArray {
                elem: ref col_elem,
                len:
                    Expr::Lit(ExprLit {
                        lit: Lit::Int(ref cols),
                        ..
                    }),
                ..
            }) = *field_type
            {
                if let Type::Array(TypeArray {
                    elem: ref scalar_elem,
                    len:
                        Expr::Lit(ExprLit {
                            lit: Lit::Int(ref rows),
                            ..
                        }),
                    ..
                }) = **col_elem
                {
                    let scalar = &*scalar_elem;
                    math::impl_matrix(&input.generics, field_type, rows, cols, name, scalar).into()
                } else {
                    panic!("Only array types are supported for columns.");
                }
            } else {
                panic!("Only arrays can implement Matrices.");
            }
        } else {
            panic!("Only newtypes are allowed to be Matrices.");
        }
    } else {
        panic!("Matrix wrappers can wrap only one array type at a time.");
    }
}

#[proc_macro]
pub fn impl_array_vectors_and_square_matrices(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    use self::math::{dimensional_type_name, VectorMatrixImpl};

    let VectorMatrixImpl {
        vector_prefix,
        covector_prefix,
        matrix_prefix,
        scalar_trait,
        real_trait: _,
        from_dim,
        to_dim,
        num_types,
    } = syn::parse(input).expect("Failed to parse input.");

    let mut tokens = TokenStream::new();
    for n in from_dim..to_dim + 1 {
        let vector = dimensional_type_name(&vector_prefix, n);
        let covector = dimensional_type_name(&covector_prefix, n);
        let matrix = dimensional_type_name(&matrix_prefix, n);

        tokens.append_all(quote! {
            #[derive(Copy, Clone, Debug, PartialEq, PartialOrd, Vector)]
            #[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
            pub struct #vector<T: #scalar_trait>(pub [T; #n]);

            #[derive(Copy, Clone, Debug, PartialEq, PartialOrd, Vector)]
            #[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
            pub struct #covector<T: #scalar_trait>(pub [T; #n]);

            // Implement vector specific operations
            impl<T: #scalar_trait> #vector<T> {
                /// Take the transpose of a vector creating a covector (row vector).
                #[inline]
                pub fn transpose(&self) -> #covector<T> {
                    #covector(self.0)
                }
            }

            // Implement covector specific operations
            impl<T: #scalar_trait> #covector<T> {
                /// Take the transpose of a covector creating a vector (column vector).
                #[inline]
                pub fn transpose(&self) -> #vector<T> {
                    #vector(self.0)
                }
            }

            // Implement inner product between vector and covector.
            impl<T: #scalar_trait> ::std::ops::Mul<#vector<T>> for #covector<T> {
                type Output = T;
                #[allow(unused_mut)]
                #[inline]
                #[unroll_for_loops]
                fn mul(self, rhs: #vector<T>) -> Self::Output {
                    let mut out = self[0] * rhs[0];
                    for i in 1..#n { out += self[i] * rhs[i]; }
                    out
                }
            }
            // for covector ref
            impl<'a, T: #scalar_trait> ::std::ops::Mul<#vector<T>> for &'a #covector<T> {
                type Output = T;
                #[allow(unused_mut)]
                #[inline]
                #[unroll_for_loops]
                fn mul(self, rhs: #vector<T>) -> Self::Output {
                    let mut out = self[0] * rhs[0];
                    for i in 1..#n { out += self[i] * rhs[i]; }
                    out
                }
            }

            // for vector ref
            impl<'a, T: #scalar_trait> ::std::ops::Mul<&'a #vector<T>> for #covector<T> {
                type Output = T;
                #[allow(unused_mut)]
                #[inline]
                #[unroll_for_loops]
                fn mul(self, rhs: &#vector<T>) -> Self::Output {
                    let mut out = self[0] * rhs[0];
                    for i in 1..#n { out += self[i] * rhs[i]; }
                    out
                }
            }
            // for vector and covector ref
            impl<'a, 'b, T: #scalar_trait> ::std::ops::Mul<&'b #vector<T>> for &'a #covector<T> {
                type Output = T;
                #[allow(unused_mut)]
                #[inline]
                #[unroll_for_loops]
                fn mul(self, rhs: &#vector<T>) -> Self::Output {
                    let mut out = self[0] * rhs[0];
                    for i in 1..#n { out += self[i] * rhs[i]; }
                    out
                }
            }
        });
        tokens.append_all(math::impl_vector_covector_mult(
            &vector,
            &covector,
            &scalar_trait,
            &matrix,
            n,
        ));
        tokens.append_all(math::impl_col_major_square_matrices(
            &matrix_prefix,
            &vector_prefix,
            &scalar_trait,
            n,
            to_dim,
        ));
        tokens.append_all(math::impl_matrix_vector_mult(
            &matrix,
            &scalar_trait,
            &vector,
            &vector,
            n,
        ));
        tokens.append_all(math::impl_covector_matrix_mult(
            &matrix,
            &scalar_trait,
            &covector,
            n,
        ));

        tokens.append_all(math::impl_left_scalar_mult(&vector, &num_types));
        tokens.append_all(math::impl_left_scalar_mult(&covector, &num_types));
        tokens.append_all(math::impl_left_scalar_mult(&matrix, &num_types));

        tokens.append_all(math::impl_norm(&vector, &scalar_trait));
        tokens.append_all(math::impl_norm(&covector, &scalar_trait));
    }
    tokens.into()
}

