/**
 * This module benchmarks the performance of internal maths against other popular libraries.
 */

#[cfg(all(feature = "unstable", test))]
mod bench {
    extern crate cgmath;
    extern crate num_traits;
    extern crate rand;
    extern crate test;
    use self::cgmath::{Matrix, SquareMatrix};
    use self::rand::{FromEntropy, IsaacRng, Rng};
    use self::test::Bencher;
    use crate::*;
    use std::ops::Mul;

    // Cgmath

    fn matrix2_cgmath() -> cgmath::Matrix2<f64> {
        let mut rng = IsaacRng::from_entropy();
        cgmath::Matrix2::new(rng.gen(), rng.gen(), rng.gen(), rng.gen())
    }

    fn matrix3_cgmath() -> cgmath::Matrix3<f64> {
        let mut rng = IsaacRng::from_entropy();
        cgmath::Matrix3::new(
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
        )
    }

    fn matrix4_cgmath() -> cgmath::Matrix4<f64> {
        let mut rng = IsaacRng::from_entropy();
        cgmath::Matrix4::new(
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
        )
    }

    fn vector2_cgmath() -> cgmath::Vector2<f64> {
        let mut rng = IsaacRng::from_entropy();
        cgmath::Vector2::new(rng.gen(), rng.gen())
    }

    fn vector3_cgmath() -> cgmath::Vector3<f64> {
        let mut rng = IsaacRng::from_entropy();
        cgmath::Vector3::new(rng.gen(), rng.gen(), rng.gen())
    }

    fn vector4_cgmath() -> cgmath::Vector4<f64> {
        let mut rng = IsaacRng::from_entropy();
        cgmath::Vector4::new(rng.gen(), rng.gen(), rng.gen(), rng.gen())
    }

    // Local maths

    fn matrix2() -> Matrix2<f64> {
        let mut rng = IsaacRng::from_entropy();
        Matrix2([[rng.gen(), rng.gen()], [rng.gen(), rng.gen()]])
    }

    fn matrix3() -> Matrix3<f64> {
        let mut rng = IsaacRng::from_entropy();
        Matrix3([
            [rng.gen(), rng.gen(), rng.gen()],
            [rng.gen(), rng.gen(), rng.gen()],
            [rng.gen(), rng.gen(), rng.gen()],
        ])
    }

    fn matrix4() -> Matrix4<f64> {
        let mut rng = IsaacRng::from_entropy();
        Matrix4([
            [rng.gen(), rng.gen(), rng.gen(), rng.gen()],
            [rng.gen(), rng.gen(), rng.gen(), rng.gen()],
            [rng.gen(), rng.gen(), rng.gen(), rng.gen()],
            [rng.gen(), rng.gen(), rng.gen(), rng.gen()],
        ])
    }

    fn matrix5() -> Matrix5<f64> {
        let mut rng = IsaacRng::from_entropy();
        Matrix5([
            [rng.gen(), rng.gen(), rng.gen(), rng.gen(), rng.gen()],
            [rng.gen(), rng.gen(), rng.gen(), rng.gen(), rng.gen()],
            [rng.gen(), rng.gen(), rng.gen(), rng.gen(), rng.gen()],
            [rng.gen(), rng.gen(), rng.gen(), rng.gen(), rng.gen()],
            [rng.gen(), rng.gen(), rng.gen(), rng.gen(), rng.gen()],
        ])
    }

    fn matrix6() -> Matrix6<f64> {
        let mut rng = IsaacRng::from_entropy();
        Matrix6([
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
        ])
    }

    fn matrix7() -> Matrix7<f64> {
        let mut rng = IsaacRng::from_entropy();
        Matrix7([
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
        ])
    }

    fn matrix8() -> Matrix8<f64> {
        let mut rng = IsaacRng::from_entropy();
        Matrix8([
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
        ])
    }

    fn matrix9() -> Matrix9<f64> {
        let mut rng = IsaacRng::from_entropy();
        Matrix9([
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
        ])
    }

    fn matrix10() -> Matrix10<f64> {
        let mut rng = IsaacRng::from_entropy();
        Matrix10([
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
        ])
    }

    fn matrix11() -> Matrix11<f64> {
        let mut rng = IsaacRng::from_entropy();
        Matrix11([
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
        ])
    }

    fn matrix12() -> Matrix12<f64> {
        let mut rng = IsaacRng::from_entropy();
        Matrix12([
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
        ])
    }

    fn matrix13() -> Matrix13<f64> {
        let mut rng = IsaacRng::from_entropy();
        Matrix13([
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
        ])
    }

    fn matrix14() -> Matrix14<f64> {
        let mut rng = IsaacRng::from_entropy();
        Matrix14([
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
        ])
    }

    fn matrix15() -> Matrix15<f64> {
        let mut rng = IsaacRng::from_entropy();
        Matrix15([
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
        ])
    }

    fn matrix16() -> Matrix16<f64> {
        let mut rng = IsaacRng::from_entropy();
        Matrix16([
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
            [
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
                rng.gen(),
            ],
        ])
    }

    fn vector2() -> Vector2<f64> {
        let mut rng = IsaacRng::from_entropy();
        Vector2([rng.gen(), rng.gen()])
    }

    fn vector3() -> Vector3<f64> {
        let mut rng = IsaacRng::from_entropy();
        Vector3([rng.gen(), rng.gen(), rng.gen()])
    }

    fn vector4() -> Vector4<f64> {
        let mut rng = IsaacRng::from_entropy();
        Vector4([rng.gen(), rng.gen(), rng.gen(), rng.gen()])
    }

    fn vector5() -> Vector5<f64> {
        let mut rng = IsaacRng::from_entropy();
        Vector5([rng.gen(), rng.gen(), rng.gen(), rng.gen(), rng.gen()])
    }

    fn vector6() -> Vector6<f64> {
        let mut rng = IsaacRng::from_entropy();
        Vector6([
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
        ])
    }

    fn vector7() -> Vector7<f64> {
        let mut rng = IsaacRng::from_entropy();
        Vector7([
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
        ])
    }

    fn vector8() -> Vector8<f64> {
        let mut rng = IsaacRng::from_entropy();
        Vector8([
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
        ])
    }
    fn vector9() -> Vector9<f64> {
        let mut rng = IsaacRng::from_entropy();
        Vector9([
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
        ])
    }

    fn vector10() -> Vector10<f64> {
        let mut rng = IsaacRng::from_entropy();
        Vector10([
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
        ])
    }

    fn vector11() -> Vector11<f64> {
        let mut rng = IsaacRng::from_entropy();
        Vector11([
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
        ])
    }

    fn vector12() -> Vector12<f64> {
        let mut rng = IsaacRng::from_entropy();
        Vector12([
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
        ])
    }

    fn vector13() -> Vector13<f64> {
        let mut rng = IsaacRng::from_entropy();
        Vector13([
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
        ])
    }

    fn vector14() -> Vector14<f64> {
        let mut rng = IsaacRng::from_entropy();
        Vector14([
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
        ])
    }
    fn vector15() -> Vector15<f64> {
        let mut rng = IsaacRng::from_entropy();
        Vector15([
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
        ])
    }
    fn vector16() -> Vector16<f64> {
        let mut rng = IsaacRng::from_entropy();
        Vector16([
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
            rng.gen(),
        ])
    }

    macro_rules! bench_uniop(
		($name: ident,  $op: ident, $gen: ident) => {
			#[bench]
			fn $name(bench: &mut Bencher) {
				let a = $gen();

				bench.iter(|| {
					test::black_box(a.$op())
				})
			}
		}
		);

    macro_rules! bench_uniop_ref(
		($name: ident,  $op: ident, $gen: ident) => {
			#[bench]
			fn $name(bench: &mut Bencher) {
				let a = &$gen();

				bench.iter(|| {
					test::black_box(a.$op())
				})
			}
		}
		);

    macro_rules! bench_binop(
		($name: ident,  $op: ident, $gen_a: ident, $gen_b: ident) => {
			#[bench]
			fn $name(bench: &mut Bencher) {
				let a = $gen_a();
				let b = $gen_b();

				bench.iter(|| {
					test::black_box(a.$op(b))
				})
			}
		}
		);

    macro_rules! bench_binop_ref(
		($name: ident,  $op: ident, $gen_a: ident, $gen_b: ident) => {
			#[bench]
			fn $name(bench: &mut Bencher) {
				let a = &$gen_a();
				let b = &$gen_b();

				bench.iter(|| {
					test::black_box(a.$op(b))
				})
			}
		}
		);

    // Bench local math library against cgmath. The performance should be similar if loop unrolling
    // works correctly.
    bench_binop!(mat4_mul_mat4_cgmath, mul, matrix4_cgmath, matrix4_cgmath);
    bench_binop!(mat3_mul_mat3_cgmath, mul, matrix3_cgmath, matrix3_cgmath);
    bench_binop!(mat2_mul_mat2_cgmath, mul, matrix2_cgmath, matrix2_cgmath);

    bench_binop!(mat4_mul_vec4_cgmath, mul, matrix4_cgmath, vector4_cgmath);
    bench_binop!(mat3_mul_vec3_cgmath, mul, matrix3_cgmath, vector3_cgmath);
    bench_binop!(mat2_mul_vec2_cgmath, mul, matrix2_cgmath, vector2_cgmath);

    bench_binop!(mat4_mul_mat4, mul, matrix4, matrix4);
    bench_binop!(mat3_mul_mat3, mul, matrix3, matrix3);
    bench_binop!(mat2_mul_mat2, mul, matrix2, matrix2);

    bench_binop!(mat4_mul_vec4, mul, matrix4, vector4);
    bench_binop!(mat3_mul_vec3, mul, matrix3, vector3);
    bench_binop!(mat2_mul_vec2, mul, matrix2, vector2);

    //	bench_binop!(vec4_dot_vec4_cgmath, dot, vector4_cgmath, vector4_cgmath);
    //	bench_binop!(vec3_dot_vec3_cgmath, dot, vector3_cgmath, vector3_cgmath);
    //	bench_binop!(vec2_dot_vec2_cgmath, dot, vector2_cgmath, vector2_cgmath);

    // Bench pass by value vs pass by reference.
    bench_binop!(mat16_mul_mat16, mul, matrix16, matrix16);
    bench_binop!(mat16_mul_vec16, mul, matrix16, vector16);
    bench_binop!(mat15_mul_mat15, mul, matrix15, matrix15);
    bench_binop!(mat15_mul_vec15, mul, matrix15, vector15);
    bench_binop!(mat14_mul_mat14, mul, matrix14, matrix14);
    bench_binop!(mat14_mul_vec14, mul, matrix14, vector14);
    bench_binop!(mat13_mul_mat13, mul, matrix13, matrix13);
    bench_binop!(mat13_mul_vec13, mul, matrix13, vector13);
    bench_binop!(mat12_mul_mat12, mul, matrix12, matrix12);
    bench_binop!(mat12_mul_vec12, mul, matrix12, vector12);
    bench_binop!(mat11_mul_mat11, mul, matrix11, matrix11);
    bench_binop!(mat11_mul_vec11, mul, matrix11, vector11);
    bench_binop!(mat10_mul_mat10, mul, matrix10, matrix10);
    bench_binop!(mat10_mul_vec10, mul, matrix10, vector10);
    bench_binop!(mat9_mul_mat9, mul, matrix9, matrix9);
    bench_binop!(mat9_mul_vec9, mul, matrix9, vector9);
    bench_binop!(mat8_mul_mat8, mul, matrix8, matrix8);
    bench_binop!(mat8_mul_vec8, mul, matrix8, vector8);
    bench_binop!(mat7_mul_mat7, mul, matrix7, matrix7);
    bench_binop!(mat7_mul_vec7, mul, matrix7, vector7);
    bench_binop!(mat6_mul_mat6, mul, matrix6, matrix6);
    bench_binop!(mat6_mul_vec6, mul, matrix6, vector6);
    bench_binop!(mat5_mul_mat5, mul, matrix5, matrix5);
    bench_binop!(mat5_mul_vec5, mul, matrix5, vector5);

    bench_binop_ref!(mat16_mul_mat16_ref, mul, matrix16, matrix16);
    bench_binop_ref!(mat16_mul_vec16_ref, mul, matrix16, vector16);
    bench_binop_ref!(mat15_mul_mat15_ref, mul, matrix15, matrix15);
    bench_binop_ref!(mat15_mul_vec15_ref, mul, matrix15, vector15);
    bench_binop_ref!(mat14_mul_mat14_ref, mul, matrix14, matrix14);
    bench_binop_ref!(mat14_mul_vec14_ref, mul, matrix14, vector14);
    bench_binop_ref!(mat13_mul_mat13_ref, mul, matrix13, matrix13);
    bench_binop_ref!(mat13_mul_vec13_ref, mul, matrix13, vector13);
    bench_binop_ref!(mat12_mul_mat12_ref, mul, matrix12, matrix12);
    bench_binop_ref!(mat12_mul_vec12_ref, mul, matrix12, vector12);
    bench_binop_ref!(mat11_mul_mat11_ref, mul, matrix11, matrix11);
    bench_binop_ref!(mat11_mul_vec11_ref, mul, matrix11, vector11);
    bench_binop_ref!(mat10_mul_mat10_ref, mul, matrix10, matrix10);
    bench_binop_ref!(mat10_mul_vec10_ref, mul, matrix10, vector10);
    bench_binop_ref!(mat9_mul_mat9_ref, mul, matrix9, matrix9);
    bench_binop_ref!(mat9_mul_vec9_ref, mul, matrix9, vector9);
    bench_binop_ref!(mat8_mul_mat8_ref, mul, matrix8, matrix8);
    bench_binop_ref!(mat8_mul_vec8_ref, mul, matrix8, vector8);
    bench_binop_ref!(mat7_mul_mat7_ref, mul, matrix7, matrix7);
    bench_binop_ref!(mat7_mul_vec7_ref, mul, matrix7, vector7);
    bench_binop_ref!(mat6_mul_mat6_ref, mul, matrix6, matrix6);
    bench_binop_ref!(mat6_mul_vec6_ref, mul, matrix6, vector6);
    bench_binop_ref!(mat5_mul_mat5_ref, mul, matrix5, matrix5);
    bench_binop_ref!(mat5_mul_vec5_ref, mul, matrix5, vector5);

    // Benchmark determinant computation against cgmath
    bench_uniop!(mat2_det, determinant, matrix2);
    bench_uniop!(mat2_det_cgmath, determinant, matrix2_cgmath);
    bench_uniop!(mat3_det, determinant, matrix3);
    bench_uniop!(mat3_det_cgmath, determinant, matrix3_cgmath);
    bench_uniop!(mat4_det, determinant, matrix4);
    bench_uniop!(mat4_det_cgmath, determinant, matrix4_cgmath);

    // Benchmark transpose computation against cgmath
    bench_uniop!(mat2_transpose, transpose, matrix2);
    bench_uniop!(mat2_transpose_cgmath, transpose, matrix2_cgmath);
    bench_uniop!(mat3_transpose, transpose, matrix3);
    bench_uniop!(mat3_transpose_cgmath, transpose, matrix3_cgmath);
    bench_uniop!(mat4_transpose, transpose, matrix4);
    bench_uniop!(mat4_transpose_cgmath, transpose, matrix4_cgmath);

    // Benchmark inverse computation against cgmath
    bench_uniop!(mat2_inverse, inverse, matrix2);
    bench_uniop!(mat2_inverse_cgmath, invert, matrix2_cgmath);
    bench_uniop!(mat3_inverse, inverse, matrix3);
    bench_uniop!(mat3_inverse_cgmath, invert, matrix3_cgmath);
    //bench_uniop!(mat4_inverse, inverse, matrix4);
    //bench_uniop!(mat4_inverse_cgmath, inverse, matrix4_cgmath);

    // Benchmark pass by value vs pass by reference
    bench_uniop_ref!(mat3_vectorize, vec, matrix3);
    bench_uniop_ref!(mat3_vectorize_ref, vec_ref, matrix3);
    bench_uniop_ref!(mat4_vectorize, vec, matrix4);
    bench_uniop_ref!(mat4_vectorize_ref, vec_ref, matrix4);

    // Benchmark fold vs sum
    #[bench]
    fn mat4_fold_sum(bench: &mut Bencher) {
        let a = matrix4();

        bench.iter(|| test::black_box(a.fold(0.0, |acc, x| x + acc)))
    }
    #[bench]
    fn mat4_sum(bench: &mut Bencher) {
        let a = matrix4();

        bench.iter(|| {
            test::black_box({
                a[0][0]
                    + a[0][1]
                    + a[0][2]
                    + a[0][3]
                    + a[1][0]
                    + a[1][1]
                    + a[1][2]
                    + a[1][3]
                    + a[2][0]
                    + a[2][1]
                    + a[2][2]
                    + a[2][3]
                    + a[3][0]
                    + a[3][1]
                    + a[3][2]
                    + a[3][3]
            })
        })
    }

    #[bench]
    fn mat4_norm_squared(bench: &mut Bencher) {
        let a = matrix4();

        bench.iter(|| test::black_box(a.norm_squared()))
    }
    #[bench]
    fn mat4_map_norm_squared(bench: &mut Bencher) {
        let a = matrix4();

        bench.iter(|| test::black_box(a.clone().map(|x| x * x).sum()))
    }
    #[bench]
    fn mat4_optimal_norm_squared(bench: &mut Bencher) {
        let a = matrix4();

        bench.iter(|| {
            test::black_box({
                a[0][0] * a[0][0]
                    + a[0][1] * a[0][1]
                    + a[0][2] * a[0][2]
                    + a[0][3] * a[0][3]
                    + a[1][0] * a[1][0]
                    + a[1][1] * a[1][1]
                    + a[1][2] * a[1][2]
                    + a[1][3] * a[1][3]
                    + a[2][0] * a[2][0]
                    + a[2][1] * a[2][1]
                    + a[2][2] * a[2][2]
                    + a[2][3] * a[2][3]
                    + a[3][0] * a[3][0]
                    + a[3][1] * a[3][1]
                    + a[3][2] * a[3][2]
                    + a[3][3] * a[3][3]
            })
        })
    }
}
